openapi: '3.0.2'
info:
  title: TODO API
  version: '1.0'

components:
  securitySchemes:
    cookieAuth:        
      type: apiKey
      in: cookie
      name: Authorization
  schemas:  
    Todo:
      type: object
      required:
        - name
      properties:
        id:
          type: string
           
        name:
          type: string
        description:
          type: string
        completed:
          type: boolean
    User:
      type: object
      required:
        - email
        - password
      properties:
        id:
          type: string
          
        email:
          type: string
          format: email
        password:
          type: string
    Response:
        type: object
        properties:
          message:
            type: string
        required:
        - message
  responses:
    NotFound:
      description: "Ressource non trouvée"
      content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
    WrongToken:
      description: "Token incorrect"
      content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
    InternalServor:
      description: "Erreur interne du serveur"
      content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
    BadRequest:
      description: "Mauvaise requête, attributs incorrects ou manquants"
      content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
    Conflict:
      description: "La requête a été rejetée"
      content:
            application/json:
              schema:
                $ref: '#/components/schemas/Response'
tags:
- name: users
  description: Représente les actions sur les utilisateurs
- name: todos
  description: Représente les actions sur les tâches

paths:
  /todos:  
    get:
      security:
        - cookieAuth: []
      
      summary: "Récupère tous les tâches de l'utilisateur"
      tags:
        - todos
      responses:
              200:
                description: "Retourne les tâches de l'utilisateur"
                content:
                  application/json:
                    schema:
                      properties:
                      type: array
                      items:
                        $ref: "#/components/schemas/Todo"
              401:
                $ref: "#/components/responses/WrongToken"
              404:
                $ref: "#/components/responses/NotFound"
              500:
                $ref: "#/components/responses/InternalServor"
    
    post:
      security:
        - cookieAuth: []
      
      summary: "Ajout d'une nouvelle tâche"
      tags:
        - todos
      requestBody:
        description: Todo
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Todo"
      responses:
              200:
                description: "Retourne la tâche créée"
                content:
                  application/json:
                    schema:
                      $ref: "#/components/schemas/Todo"
              400:
                $ref: "#/components/responses/BadRequest"
              401:
                $ref: "#/components/responses/WrongToken"
              404:
                $ref: "#/components/responses/NotFound"
              500:
                $ref: "#/components/responses/InternalServor"

  /todos/{id}:
    get:
        security:
          - cookieAuth: []
        summary: "Récupère une tâche via son id"
        tags:
          - todos
        parameters:
          - in: path
            name: "id"
            required: true
            schema:
              type: string  
        responses:
          200:
            description: "Retourne la tâche créée"
            content:
              application/json:
                schema:
                  $ref: "#/components/schemas/Todo"
          400:
            $ref: "#/components/responses/BadRequest"
          401:
            $ref: "#/components/responses/WrongToken"
          404:
            $ref: "#/components/responses/NotFound"
          500:
            $ref: "#/components/responses/InternalServor"

    put:
      security:
        - cookieAuth: []
      
      summary: "Modification d'une tâche"
      tags:
        - todos
      parameters:
        - in: path
          name: "id"
          required: true
          schema:
            type: string
      requestBody:
        description: Todo
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Todo"
      responses:
        200:
          description: "Retourne ok si modifiée"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Response"
        400:
          $ref: "#/components/responses/BadRequest"
        401:
          $ref: "#/components/responses/WrongToken"
        404:
          $ref: "#/components/responses/NotFound"
        500:
          $ref: "#/components/responses/InternalServor"
    
    delete:
      security:
        - cookieAuth: []
      tags:
        - todos
      summary: "Supression d'une tâche"
      parameters:
        - in: path
          name: "id"
          required: true
          schema:
            type: string
            
      responses:
        200:
          description: "Retourne ok si supprimée"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Response"
        400:
          $ref: "#/components/responses/BadRequest"
        401:
          $ref: "#/components/responses/WrongToken"
        404:
          $ref: "#/components/responses/NotFound"
        500:
          $ref: "#/components/responses/InternalServor"
  
  /login:
    post:
      summary: "Connexion"
      tags:
        - users
      requestBody:
        description: User
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
      responses:
              200:
                description: "Retourne l'utilisateur connecté"
                content:
                  application/json:
                    schema:
                      $ref: "#/components/schemas/User"
              400:
                $ref: "#/components/responses/BadRequest"
              404:
                $ref: "#/components/responses/NotFound"
              409:
                $ref: "#/components/responses/Conflict"
              500:
                $ref: "#/components/responses/InternalServor"

  /logout:
    post:
      summary: "Déconnexion"
      tags:
      - users
      responses:
              200:
                description: "Retourne l'utilisateur"
                content:
                  application/json:
                    schema:
                      $ref: "#/components/schemas/User"
              400:
                $ref: "#/components/responses/BadRequest"
              404:
                $ref: "#/components/responses/NotFound"
              409:
                $ref: "#/components/responses/Conflict"
              500:
                $ref:  "#/components/responses/InternalServor"
  
  /signup:
    post:
      summary: "Créer un compte"
      tags:
      - users
      requestBody:
        description: User
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/User"
      responses:
              200:
                description: "Retourne l'utilisateur connecté"
                content:
                  application/json:
                    schema:
                      $ref: "#/components/schemas/User"
              400:
                $ref: "#/components/responses/BadRequest"
              404:
                $ref: "#/components/responses/NotFound"
              409:
                $ref: "#/components/responses/Conflict"
              500:
                $ref: "#/components/responses/InternalServor"
