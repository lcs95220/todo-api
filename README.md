# API TODO

Un exemple d'application type Todo implémentée avec NodeJs/Express/Typescript.

Les données sont persistées dans un container docker mongodb.
L'application peut se lancer de manière classique avec npm et également via docker.

Des tests automatisés sont rédigés avec Jest.

## Technologies utilisées
![](https://img.shields.io/badge/-Node.js-339933?style=for-the-badge&logo=Node.js&logoColor=fff)
![](https://img.shields.io/badge/-NPM-CB3837?style=for-the-badge&logo=NPM&logoColor=fff)
![](https://img.shields.io/badge/-TypeScript-007ACC?style=for-the-badge&logo=TypeScript&logoColor=fff)
![](https://img.shields.io/badge/-Express-F8F8F5?style=for-the-badge)
![](https://img.shields.io/badge/-Nodemon-76D04B?style=for-the-badge&logo=Nodemon&logoColor=fff)
![](https://img.shields.io/badge/-ESLint-4B32C3?style=for-the-badge&logo=ESLint&logoColor=fff)
![](https://img.shields.io/badge/-Prettier-F7B93E?style=for-the-badge&logo=Prettier&logoColor=000)
![](https://img.shields.io/badge/-Jest-C21325?style=for-the-badge&logo=Jest&logoColor=fff)
![](https://img.shields.io/badge/-Swagger-85EA2D?style=for-the-badge&logo=Swagger&logoColor=000)
![](https://img.shields.io/badge/-Docker-2496ED?style=for-the-badge&logo=Docker&logoColor=fff)
![](https://img.shields.io/badge/-MongoDB-47A248?style=for-the-badge&logo=MongoDB&logoColor=fff)

Icones provenant de [simpleicons](https://simpleicons.org/)

# Installation

A la racine du projet : 

- ```npm install```
- Modifier les variables d'environnement dans le fichier *.env.example* et renommer *.env*

## Lancement de l'application :

* ### Sans docker :

    - Il faut au préalable avoir mongodb d'installé et lancer le serveur
    - Modifier les variables d'environnement si besoin
    - ``` npm run dev``` # environnement de développement
    - ``` npm start``` # environnement de production

<br>

* ### Avec docker :

    Dans le fichier *docker-compose.yml*, modifier les variables d'environnement du container mongo de manière a ce qu'elles aient les mêmes valeurs que le fichier *.env* du projet.

    1. Environnement de développement
    
        - ```docker-compose up```

    2. Environnement de production
    
        - Dans le fichier *docker-compose.yml*, modifier la valeur **Dockerfile.dev** par **Dockerfile**
        - ```docker-compose up```

    
## Tests

Afin d'exécuter les tests :
    ``` npm run test```

## Documentation

Une documentation Swagger rédigée dans le fichier *swagger.yml* est disponible à l'url */docs*

## Qualité de code

Eslint et Prettier sont utilisés pour la qualité du code.
Les configurations respectives sont disponibles dans les fichiers *.eslintrc* et *.prettierrc*.

## Contact

<lucas.hilaire.74@gmail.com>