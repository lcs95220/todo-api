import compression from "compression";
import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import helmet from "helmet";
import hpp from "hpp";
import { connect, set } from "mongoose";
import morgan from "morgan";
import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
import { dbConnection } from "./database";
import Routes from "./interfaces/routes.interface";
import errorMiddleware from "./middlewares/error.middleware";
import { logger, stream } from "./utils/logger";

class App {
    public app: express.Application;
    public port: string | number;
    public env: string;

    constructor() {
        this.app = express();
        this.port = process.env.PORT || 3000;
        this.env = process.env.NODE_ENV || "development";
    }

    public async build(routes: Routes[]): Promise<App> {
        this.connectToDatabase();
        this.initializeMiddlewares();
        this.initializeRoutes(routes);
        this.initializeSwagger();
        this.initializeErrorHandling();
        return this;
    }

    public listen() {
        this.app.listen(this.port, () => {
            logger.info(`🚀 Serveur lancé !`);
            logger.info(`${process.env.DOMAIN_URL}:${this.port}`);
        });
    }

    public getServer() {
        return this.app;
    }

    private async connectToDatabase() {
        if (this.env !== "production") {
            set("debug", true);
        }

        try {
            await connect(dbConnection.url, dbConnection.options);
            logger.info("🟢 Base de données connectée");
        } catch (error: any) {
            logger.error(`🔴 Impossible de se connecter à la base de données : ${error}.`);
        }
    }

    private initializeMiddlewares() {
        if (this.env === "production") {
            this.app.use(morgan("combined", { stream }));
            this.app.use(cors({ origin: process.env.DOMAIN_URL || "", credentials: true }));
        } else if (this.env === "development") {
            this.app.use(morgan("dev", { stream }));
            this.app.use(cors({ origin: true, credentials: true }));
        }

        this.app.use(hpp());
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(cookieParser());
    }

    private initializeRoutes(routes: Routes[]) {
        routes.forEach((route) => {
            this.app.use("/", route.router);
        });
    }

    private initializeSwagger() {
        const options = {
            definition: {
                openapi: "3.0.0",
                info: {
                    title: "Todo API",
                    version: "1.0.0",
                    description: "Documentation de l'api TodoApp",
                },
            },
            apis: ["swagger.yaml"],
        };

        const specs = swaggerJSDoc(options);
        this.app.use("/docs", swaggerUi.serve, swaggerUi.setup(specs));
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }
}

export default App;
