import { Router } from "express";
import TodoController from "../controllers/todos.controller";
import Route from "../interfaces/routes.interface";
import validationMiddleware from "../middlewares/validation.middleware";
import authMiddleware from "../middlewares/auth.middleware";
import { CreateTodoDto } from "../dtos/todos.dto";

class TodosRoute implements Route {
    public path = "/todos";
    public router = Router();
    public todosController = new TodoController();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, authMiddleware, this.todosController.getAllTodos);
        this.router.get(`${this.path}/:id`, authMiddleware, this.todosController.getTodoById);
        this.router.post(`${this.path}`, authMiddleware, validationMiddleware(CreateTodoDto, "body", true), this.todosController.createTodo);
        this.router.put(`${this.path}/:id`, authMiddleware, validationMiddleware(CreateTodoDto, "body", true), this.todosController.updateTodo);
        this.router.delete(`${this.path}/:id`, authMiddleware, this.todosController.deleteTodo);
    }
}

export default TodosRoute;
