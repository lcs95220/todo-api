import { NextFunction, Request, Response } from "express";
import { SuccessResponse } from "../http_responses/HttpResponse";

class IndexController {
    public index = (req: Request, res: Response, next: NextFunction) => {
        try {
            return new SuccessResponse(res, "Bienvenue sur l'api todo_app");
        } catch (error) {
            next(error);
        }
    };
}

export default IndexController;
