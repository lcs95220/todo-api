import { NextFunction, Request, Response } from "express";
import { CreatedResponse, SuccessResponse } from "../http_responses/HttpResponse";
import { User } from "../interfaces/users.interface";
import userService from "../services/users.service";
import { logger } from "../utils/logger";

class UsersController {
    public userService = new userService();

    public getUsers = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const findAllUsersData: User[] = await this.userService.findAllUser();
            return new SuccessResponse(res, findAllUsersData);
        } catch (error) {
            logger.error(error);
            next(error);
        }
    };

    public getUserById = async (req: Request, res: Response, next: NextFunction) => {
        const userId: string = req.params.id;

        try {
            const findOneUserData: User = await this.userService.findUserById(userId);
            return new SuccessResponse(res, findOneUserData);
        } catch (error) {
            next(error);
        }
    };

    public createUser = async (req: Request, res: Response, next: NextFunction) => {
        const userData: User = req.body;

        try {
            const createUserData: User = await this.userService.createUser(userData);
            return new CreatedResponse(res, createUserData);
        } catch (error) {
            next(error);
        }
    };

    public updateUser = async (req: Request, res: Response, next: NextFunction) => {
        const userId: string = req.params.id;
        const userData: User = req.body;

        try {
            const updateUserData: User = await this.userService.updateUser(userId, userData);
            return new SuccessResponse(res, updateUserData);
        } catch (error) {
            next(error);
        }
    };

    public deleteUser = async (req: Request, res: Response, next: NextFunction) => {
        const userId: string = req.params.id;

        try {
            const deleteUserData: User = await this.userService.deleteUserData(userId);
            return new SuccessResponse(res, deleteUserData);
        } catch (error) {
            next(error);
        }
    };
}

export default UsersController;
