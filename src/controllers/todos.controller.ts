import { NextFunction, Response } from "express";
import { CreatedResponse, SuccessResponse } from "../http_responses/HttpResponse";
import { RequestWithUser } from "../interfaces/auth.interface";
import Todo from "../interfaces/todos.interface";
import todoService from "../services/todos.service";
import { logger } from "../utils/logger";

class TodoController {
    public todoService = new todoService();

    public getAllTodos = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        try {
            const findAllTodos: Todo[] = await this.todoService.findAllTodos(req.user._id);
            return new SuccessResponse(res, findAllTodos);
        } catch (error) {
            logger.error(error);
            next(error);
        }
    };

    public getTodoById = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        const todoId: string = req.params.id;
        try {
            const findOneTodo: Todo = await this.todoService.findOneTodo(req.user._id, todoId);
            return new SuccessResponse(res, findOneTodo);
        } catch (error) {
            logger.error(error);
            next(error);
        }
    };

    public createTodo = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        const todoData: Todo = req.body;
        try {
            const createTodoData: Todo = await this.todoService.createTodo(todoData, req.user._id);
            return new CreatedResponse(res, createTodoData);
        } catch (error) {
            next(error);
        }
    };

    public updateTodo = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        const todoId: string = req.params.id;
        const todoData: Todo = req.body;
        try {
            const updateTodoData: string = await this.todoService.updateTodo(todoData, req.user._id, todoId);
            return new SuccessResponse(res, updateTodoData);
        } catch (error) {
            next(error);
        }
    };

    public deleteTodo = async (req: RequestWithUser, res: Response, next: NextFunction) => {
        const todoId: string = req.params.id;
        try {
            const deleteTodoData: string = await this.todoService.deleteTodo(req.user._id, todoId);
            return new SuccessResponse(res, deleteTodoData);
        } catch (error) {
            next(error);
        }
    };
}

export default TodoController;
