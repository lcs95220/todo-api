import { model, Schema } from "mongoose";
import Todo from "../interfaces/todos.interface";

const todoSchema: Schema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        description: {
            type: String,
        },
        completed: {
            type: Boolean,
            default: false,
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: "User",
        },
    },
    { timestamps: true },
);

const todoModel = model<Todo>("Todo", todoSchema);

export default todoModel;
