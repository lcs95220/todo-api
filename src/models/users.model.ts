import { model, Schema, Document } from "mongoose";
import { User } from "../interfaces/users.interface";

const userSchema: Schema = new Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});

userSchema.index({ email: 1 }, { unique: true });

const userModel = model<User & Document>("User", userSchema);

export default userModel;
