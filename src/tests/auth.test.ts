import bcrypt from "bcrypt";
import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import request from "supertest";
import App from "../app";
import { CreateUserDto } from "../dtos/users.dto";
import { ConflictException } from "../http_responses/HttpException";
import { TokenData } from "../interfaces/auth.interface";
import userModel from "../models/users.model";
import AuthRoute from "../routes/auth.route";
import UsersRoute from "../routes/users.route";
import AuthService from "../services/auth.service";
import API_MESSAGE from "../utils/apiMessage";

describe("Testing AuthController", () => {
    let app: App;
    let authRoute: AuthRoute;
    let mongoServer: MongoMemoryServer;
    let token = "";

    const userData = {
        _id: "602ab9273a828c0029835086",
        email: "test@test.com",
        password: "test",
    };

    beforeAll(async () => {
        mongoServer = new MongoMemoryServer();
        const URI = await mongoServer.getUri();

        mongoose.connect(URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
        });
        process.env.JWT_SECRET = "jwt-test";
        authRoute = new AuthRoute();
        app = await new App().build([new UsersRoute(), authRoute]);
    });

    async function login() {
        await userModel.create({ ...userData, password: await bcrypt.hash(userData.password, 10) });

        const res = await request(app.getServer()).post("/login").send(userData);
        const cookies = res.headers["set-cookie"];
        token = cookies.split(";")[0];
    }

    afterAll(async (done) => {
        mongoose.disconnect(done);
        await mongoServer.stop();
    });

    afterEach(async () => {
        const collections = await mongoose.connection.db.collections();
        for (const collection of collections) {
            await collection.deleteMany({});
        }
    });

    describe("POST /signup", () => {
        it("response should have the Create userData", async () => {
            const userData: CreateUserDto = {
                email: "test@email.com",
                password: "q1w2e3r4!",
            };

            authRoute.authController.authService.users.findOne = jest.fn().mockReturnValue(Promise.resolve(undefined));

            authRoute.authController.authService.users.create = jest.fn().mockReturnValue({ _id: 0, ...userData });

            return request(app.getServer()).post("/signup").send(userData);
        });
    });

    describe("POST /login", () => {
        it("response should have the Set-Cookie header with the Authorization token", async () => {
            const userData: CreateUserDto = {
                email: "test@email.com",
                password: "q1w2e3r4!",
            };
            process.env.JWT_SECRET = "jwt_secret";

            authRoute.authController.authService.users.findOne = jest.fn().mockReturnValue(
                Promise.resolve({
                    _id: 0,
                    email: "test@email.com",
                    password: await bcrypt.hash(userData.password, 10),
                }),
            );
            return request(app.getServer())
                .post("/login")
                .send(userData)
                .expect("Set-Cookie", /^Authorization=.+/);
        });
    });

    describe("POST /logout", () => {
        it("logout Set-Cookie Authorization=; Max-age=0", async () => {
            await login();
            const result = await request(app.getServer()).post("/logout").set("Cookie", token);
            expect(result.status).toBe(200);
            expect(result.header["set-cookie"][0]).toEqual("Authorization=; Max-age=0");
        });
    });
});

describe("Testing AuthService", () => {
    describe("when creating a cookie", () => {
        it("should return a string", () => {
            const tokenData: TokenData = {
                token: "",
                expiresIn: 1,
            };

            const authService = new AuthService();

            expect(typeof authService.createCookie(tokenData)).toEqual("string");
        });
    });

    describe("when registering a user", () => {
        describe("if the email is already token", () => {
            it("should throw an error", async () => {
                const userData: CreateUserDto = {
                    email: "test@email.com",
                    password: "q1w2e3r4!",
                };

                const authService = new AuthService();

                authService.users.findOne = jest.fn().mockReturnValue(Promise.resolve(userData));
                await expect(authService.signup(userData)).rejects.toMatchObject(new ConflictException(API_MESSAGE.EMAIL_ALREADY_EXISTS));
            });
        });

        describe("if the email is not token", () => {
            it("should not throw an error", async () => {
                const userData: CreateUserDto = {
                    email: "test@email.com",
                    password: "q1w2e3r4!",
                };
                process.env.JWT_SECRET = "jwt_secret";

                const authService = new AuthService();
                authService.users.findOne = jest.fn().mockReturnValue(Promise.resolve(undefined));

                authService.users.create = jest.fn().mockReturnValue({ _id: 0, ...userData });

                await expect(authService.signup(userData)).resolves.toBeDefined();
            });
        });
    });
});
