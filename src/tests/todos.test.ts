import bcrypt from "bcrypt";
import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import request from "supertest";
import App from "../app";
import todoModel from "../models/todos.model";
import userModel from "../models/users.model";
import AuthRoute from "../routes/auth.route";
import TodosRoute from "../routes/todos.route";
import UsersRoute from "../routes/users.route";

describe("Testing Todos", () => {
    let app: App;
    let todosRoute: TodosRoute;
    let authRoute: AuthRoute;
    let mongoServer: MongoMemoryServer;
    let token = "";

    const todoData = [
        {
            _id: "602ab9273a828c0029837434",
            user: "602ab9273a828c0029835086",
            name: "Todo test",
        },
        {
            _id: "602ab9273a828c0029837435",
            user: "602ab9273a828c0029835086",
            name: "Todo test 2",
            completed: true,
        },
    ];

    const userData = {
        _id: "602ab9273a828c0029835086",
        email: "test@test.com",
        password: "test",
    };

    beforeAll(async () => {
        mongoServer = new MongoMemoryServer();
        const URI = await mongoServer.getUri();

        await mongoose.connect(URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
        });
        process.env.JWT_SECRET = "jwt-test";
        todosRoute = new TodosRoute();
        authRoute = new AuthRoute();
        app = await new App().build([todosRoute, new UsersRoute(), authRoute]);
    });

    beforeEach(async () => {
        await userModel.create({ ...userData, password: await bcrypt.hash(userData.password, 10) });

        await todoModel.insertMany(todoData);
        const res = await request(app.getServer()).post("/login").send(userData);
        let cookies = res.headers["set-cookie"];
        if (cookies.length > 0) {
            cookies = cookies[0];
        }
        token = cookies.split(";")[0];
    });

    afterAll(async (done) => {
        mongoose.disconnect(done);
        await mongoServer.stop();
    });

    afterEach(async () => {
        const collections = await mongoose.connection.db.collections();
        for (const collection of collections) {
            await collection.deleteMany({});
        }
    });

    describe("GET /todos", () => {
        it("response All Todos", async () => {
            const result = await request(app.getServer()).get(`${todosRoute.path}`).set("Cookie", token);
            checkFindTodo(result, true);
        });
        it("should fail because no token provided", async () => {
            const result = await request(app.getServer()).get(`${todosRoute.path}`);
            expect(result.status).toBe(404);
        });
    });

    describe("GET /todos/:id", () => {
        it("response One Todo", async () => {
            const result = await request(app.getServer()).get(`${todosRoute.path}/${todoData[0]._id}`).set("Cookie", token);
            checkFindTodo(result);
        });
        it("should fail because no token provided", async () => {
            const result = await request(app.getServer()).get(`${todosRoute.path}/${todoData[0]._id}`);
            expect(result.status).toBe(404);
        });
    });
    function checkFindTodo(result, all = false) {
        expect(result.status).toBe(200);
        expect(result.body.data);
        if (all) expect(result.body.data.length).toBeGreaterThan(0);
        const todosResult = all ? result.body.data[0] : result.body.data;
        expect(todosResult.name).toEqual(todoData[0].name);
    }
});
