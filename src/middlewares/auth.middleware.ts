import { NextFunction, Response } from "express";
import jwt from "jsonwebtoken";
import { NotFoundException, UnAuthorizedException } from "../http_responses/HttpException";
import { DataStoredInToken, RequestWithUser } from "../interfaces/auth.interface";
import userModel from "../models/users.model";
import API_MESSAGE from "../utils/apiMessage";

const authMiddleware = async (req: RequestWithUser, res: Response, next: NextFunction) => {
    try {
        const cookies = req.cookies;

        if (cookies && cookies.Authorization) {
            const secret = process.env.JWT_SECRET;
            const verificationResponse = jwt.verify(cookies.Authorization, secret) as DataStoredInToken;
            const userId = verificationResponse._id;
            const findUser = await userModel.findById(userId);

            if (findUser) {
                req.user = findUser;
                next();
            } else {
                next(new UnAuthorizedException(API_MESSAGE.WRONG_TOKEN));
            }
        } else {
            next(new NotFoundException(API_MESSAGE.NO_TOKEN));
        }
    } catch (error) {
        next(new UnAuthorizedException(API_MESSAGE.WRONG_TOKEN));
    }
};

export default authMiddleware;
