import { NextFunction, Request, Response } from "express";
import { HttpException } from "../http_responses/HttpException";
import API_MESSAGE from "../utils/apiMessage";
import { logger } from "../utils/logger";

const errorMiddleware = (error: HttpException, req: Request, res: Response, next: NextFunction) => {
    try {
        const status: number = error.status || 500;
        const message: string = error.message || API_MESSAGE.GLOBAL_ERROR;
        logger.error(`StatusCode : ${status}, Message : ${message}`);
        res.status(status).json({ message });
    } catch (error) {
        next(error);
    }
};

export default errorMiddleware;
