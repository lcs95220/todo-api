export class HttpException extends Error {
    public status: number;
    public message: any;

    constructor(status: number, message: any) {
        super(message);
        this.status = status;
        this.message = message;
    }
}

export class NotFoundException extends HttpException {
    public message: any;
    constructor(message: any) {
        super(404, message);
    }
}

export class BadRequestException extends HttpException {
    public message: any;
    constructor(message: any) {
        super(400, message);
    }
}

export class UnAuthorizedException extends HttpException {
    public message: any;
    constructor(message: any) {
        super(401, message);
    }
}

export class ConflictException extends HttpException {
    public message: any;
    constructor(message: any) {
        super(409, message);
    }
}

export class InternalErrorException extends HttpException {
    public message: any;
    constructor(message: any) {
        super(500, message);
    }
}
