import { Response } from "express";

abstract class HttpResponse {
    public message: any;
    constructor(res: Response, status: number, message: any) {
        const display: any = typeof message === "string" ? { message } : message;
        res.status(status).json(display);
    }
}

export class CreatedResponse extends HttpResponse {
    constructor(res: Response, message: any) {
        super(res, 201, message);
    }
}

export class SuccessResponse extends HttpResponse {
    constructor(res: Response, message: any) {
        super(res, 200, message);
    }
}
