import App from "./app";
import AuthRoute from "./routes/auth.route";
import IndexRoute from "./routes/index.route";
import TodosRoute from "./routes/todos.route";
import UsersRoute from "./routes/users.route";
import validateEnv from "./utils/validateEnv";

validateEnv();

const routes = [new IndexRoute(), new UsersRoute(), new AuthRoute(), new TodosRoute()];
const app = new App();
app.build(routes);
app.listen();
