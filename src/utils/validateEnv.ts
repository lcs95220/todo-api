import { cleanEnv, port, str } from "envalid";
import fs from "fs";
import dotenv from "dotenv";

const validateEnv = () => {
    if (fs.existsSync(".env")) {
        dotenv.config({ path: ".env" });
    } else {
        dotenv.config({ path: ".env.example" });
    }

    cleanEnv(process.env, {
        NODE_ENV: str(),
        PORT: port(),
        MONGO_HOST: str(),
        MONGO_PORT: str(),
        MONGO_DATABASE: str(),
        JWT_SECRET: str(),
        LOGS_ROTATE: str(),
    });
};

export default validateEnv;
