const API_MESSAGE = {
    USER_NOT_FOUND: "Utilisateur non trouvé",
    EMAIL_NOT_FOUND: "Adresse mail non trouvée",
    EMAIL_ALREADY_EXISTS: "Adresse mail déjà existante",
    EMPTY_DATA: "Données vides",
    INCORRECT_PASSWORD: "Mot de passe incorrect",
    WRONG_TOKEN: "Token d'authentification incorrect",
    NO_TOKEN: "Aucun token d'authentification fourni",
    GLOBAL_ERROR: "Erreur rencontrée",
    DATA_NOT_FOUND: "Donnée non trouvée",
    DATA_DELETED: "Donnée supprimée",
    DATA_UPDATED: "Donnée modifiée",
};

export default API_MESSAGE;
