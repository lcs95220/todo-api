import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { CreateUserDto } from "../dtos/users.dto";
import { BadRequestException, ConflictException } from "../http_responses/HttpException";
import { DataStoredInToken, TokenData } from "../interfaces/auth.interface";
import { User } from "../interfaces/users.interface";
import userModel from "../models/users.model";
import API_MESSAGE from "../utils/apiMessage";
import { isEmpty } from "../utils/util";

class AuthService {
    public users = userModel;

    public async signup(userData: CreateUserDto): Promise<User> {
        if (isEmpty(userData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);

        const findUser: User = await this.users.findOne({ email: userData.email });
        if (findUser) throw new ConflictException(API_MESSAGE.EMAIL_ALREADY_EXISTS);

        const hashedPassword = await bcrypt.hash(userData.password, 10);
        const createUserData: User = await this.users.create({ ...userData, password: hashedPassword });

        return createUserData;
    }

    public async login(userData: CreateUserDto): Promise<{ cookie: string; findUser: User }> {
        if (isEmpty(userData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);

        const findUser: User = await this.users.findOne({ email: userData.email });
        if (!findUser) throw new ConflictException(API_MESSAGE.EMAIL_NOT_FOUND);

        const isPasswordMatching: boolean = await bcrypt.compare(userData.password, findUser.password);
        if (!isPasswordMatching) throw new ConflictException(API_MESSAGE.INCORRECT_PASSWORD);

        const tokenData = this.createToken(findUser);
        const cookie = this.createCookie(tokenData);

        return { cookie, findUser };
    }

    public async logout(userData: User): Promise<User> {
        if (isEmpty(userData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);

        const findUser: User = await this.users.findOne({ password: userData.password });
        if (!findUser) throw new ConflictException(API_MESSAGE.USER_NOT_FOUND);

        return findUser;
    }

    public createToken(user: User): TokenData {
        const dataStoredInToken: DataStoredInToken = { _id: user._id };
        const secret: string = process.env.JWT_SECRET;
        const expiresIn: number = 60 * 60;

        return { expiresIn, token: jwt.sign(dataStoredInToken, secret, { expiresIn }) };
    }

    public createCookie(tokenData: TokenData): string {
        return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`;
    }
}

export default AuthService;
