import { BadRequestException, NotFoundException } from "../http_responses/HttpException";
import todoModel from "../models/todos.model";
import { isEmpty } from "../utils/util";
import Todo from "../interfaces/todos.interface";
import API_MESSAGE from "../utils/apiMessage";

class TodoService {
    public todos = todoModel;

    public async findAllTodos(userId: string): Promise<Todo[]> {
        const todos: Todo[] = await this.todos.find({ user: userId });
        return todos;
    }

    public async findOneTodo(userId: string, todoId: string): Promise<Todo> {
        const todo: Todo = await this.todos.findOne({ user: userId, _id: todoId });
        if (!todo) {
            throw new NotFoundException(API_MESSAGE.DATA_NOT_FOUND);
        }
        return todo;
    }

    public async createTodo(todoData: Todo, userId: string): Promise<Todo> {
        if (isEmpty(todoData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);
        const createTodoData: Todo = await this.todos.create({ ...todoData, user: userId });
        return createTodoData;
    }

    public async updateTodo(todoData: Todo, userId: string, todoId: string): Promise<string> {
        if (isEmpty(todoData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);
        const result = await this.todos.updateOne({ user: userId, _id: todoId }, todoData);
        if ((result?.nModified ?? 0) === 0) {
            throw new NotFoundException(API_MESSAGE.DATA_NOT_FOUND);
        }
        return API_MESSAGE.DATA_UPDATED;
    }

    public async deleteTodo(userId: string, todoId: string): Promise<string> {
        const deleteTodoData = await this.todos.deleteOne({ user: userId, _id: todoId });
        if (deleteTodoData.deletedCount === 0) {
            throw new NotFoundException(API_MESSAGE.DATA_NOT_FOUND);
        }
        return API_MESSAGE.DATA_DELETED;
    }
}

export default TodoService;
