import bcrypt from "bcrypt";
import { BadRequestException, ConflictException } from "../http_responses/HttpException";
import { User } from "../interfaces/users.interface";
import userModel from "../models/users.model";
import API_MESSAGE from "../utils/apiMessage";
import { isEmpty } from "../utils/util";

class UserService {
    public users = userModel;

    public async findAllUser(): Promise<User[]> {
        const users: User[] = await this.users.find();
        return users;
    }

    public async findUserById(userId: string): Promise<User> {
        const findUser: User = await this.users.findOne({ _id: userId });
        if (!findUser) throw new ConflictException("Utilisateur non trouvé");

        return findUser;
    }

    public async createUser(userData: User): Promise<User> {
        if (isEmpty(userData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);

        const findUser: User = await this.users.findOne({ email: userData.email });
        if (findUser) throw new ConflictException(API_MESSAGE.EMAIL_ALREADY_EXISTS);

        const hashedPassword = await bcrypt.hash(userData.password, 10);
        const createUserData: User = await this.users.create({ ...userData, password: hashedPassword });
        return createUserData;
    }

    public async updateUser(userId: string, userData: User): Promise<User> {
        if (isEmpty(userData)) throw new BadRequestException(API_MESSAGE.EMPTY_DATA);

        const hashedPassword = await bcrypt.hash(userData.password, 10);
        const updateUserById: User = await this.users.findByIdAndUpdate(userId, { ...userData, password: hashedPassword });
        if (!updateUserById) throw new ConflictException(API_MESSAGE.USER_NOT_FOUND);

        return updateUserById;
    }

    public async deleteUserData(userId: string): Promise<User> {
        const deleteUserById: User = await this.users.findByIdAndDelete(userId);
        if (!deleteUserById) throw new ConflictException(API_MESSAGE.USER_NOT_FOUND);

        return deleteUserById;
    }
}

export default UserService;
