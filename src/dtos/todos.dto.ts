import { IsString, IsBoolean, IsMongoId } from "class-validator";

export class CreateTodoDto {
    @IsString()
    public name: string;

    @IsString()
    public description?: string;

    @IsBoolean()
    public completed?: boolean;

    @IsMongoId()
    public user: string;
}
