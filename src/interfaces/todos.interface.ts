import { Document } from "mongoose";

export default interface Todo extends Document {
    name: string;
    description?: string;
    completed?: string;
    user: string;
}
